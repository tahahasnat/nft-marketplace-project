import { useEffect, useState } from 'react'
import { ethers } from 'ethers'
import { useRouter } from 'next/router'
import axios from 'axios'
import Web3Modal from 'web3modal'

import {
  marketplaceAddress
} from '../config'

import nftMarket from '../build/contracts/nftMarket.json'

export default function ResellNFT() {
  const [formInput, updateFormInput] = useState({ price: '', image: '' })
  const router = useRouter()
  const { id, tokenURI } = router.query
  const { image, price } = formInput

  useEffect(() => {
    fetchNFT()
  }, [id])

  async function fetchNFT() {
    if (!tokenURI) return
    const meta = await axios.get(tokenURI)
    updateFormInput(state => ({ ...state, image: meta.data.image }))
  }

  async function sellNft() {
    if (!price) return
    const web3Modal = new Web3Modal()
    const connection = await web3Modal.connect()
    const provider = new ethers.providers.Web3Provider(connection)
    const signer = provider.getSigner()

    const priceFormatted = ethers.utils.parseUnits(formInput.price, 'ether')
    let contract = new ethers.Contract(marketplaceAddress, nftMarket.abi, signer)
    let listingPrice = await contract.getNftListingFee()

    listingPrice = listingPrice.toString()
    let transaction = await contract.sellboughtNFT(id, priceFormatted, { value: listingPrice })
    await transaction.wait()

    router.push('/')
  }
  return (
    <div className="flex justify-center">
      <div className="w-1/5 flex flex-col ">
        <input
          placeholder="Asset Price in Eth"
          className="mt-2 border rounded p-4"
          onChange={e => updateFormInput({ ...formInput, price: e.target.value })}
        />
        {
          image && (
            <img className=" mt-4" width="350" src={image} />
          )
        }
        <button onClick={sellNft} className="font-bold mt-4 bg-gray-500 p-4">
          Sell NFT
        </button>
      </div>
    </div>
  )
}
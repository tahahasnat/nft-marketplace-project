import '../styles/globals.css'
import Link from 'next/link'

  
import React, {useState, useEffect} from 'react'


function MyApp({ Component, pageProps }) {

  let [account, setAccount] = useState([])
  useEffect(() => {
    loadAccount();
  }, [account]);

let loadAccount = async () => {

  const accounts = await window.ethereum.request({
    method: "eth_requestAccounts",
  });
  setAccount(accounts[0]);
}
console.log(account)
  return (
    <div className= "bg-gray-200 ">
      <nav className="border-b p-6  bg-gray-100 ">
        <p className="text-4xl flex mt-4  justify-center font-bold ">NFT Marketplace</p>
        <p className="text-4xl flex mt-4  justify-center font-bold ">wallet Address: {account}</p>
        <div className="flex mt-4  justify-center"><Link href="/"><a className="mr-4 ">MarketPlace</a></Link>
          <Link href="/mintNft"><a className="mr-4 ">Mint NFT</a></Link>
          <Link href="/myNFTs"><a className="mr-4 ">My NFT</a></Link>
        </div>
      </nav>
      <Component {...pageProps} />
    </div>
  )
}

export default MyApp

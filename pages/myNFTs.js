import { ethers } from 'ethers'
import { useEffect, useState } from 'react'
import axios from 'axios'
import Web3Modal from 'web3modal'
import { useRouter } from 'next/router'

import {
  marketplaceAddress
} from '../config'

import nftMarket from '../build/contracts/nftMarket.json'

export default function MyAssets() {
  const [nfts, setNfts] = useState([])
  const [loadingState, setLoadingState] = useState('not-loaded')
  const router = useRouter()
  useEffect(() => {
    loadNFTs()
  }, [])
  async function loadNFTs() {
    const web3Modal = new Web3Modal({
      network: "mainnet",
      cacheProvider: true,
    })
    const connection = await web3Modal.connect()
    const provider = new ethers.providers.Web3Provider(connection)
    const signer = provider.getSigner()

    const marketplaceContract = new ethers.Contract(marketplaceAddress, nftMarket.abi, signer)
    const data = await marketplaceContract.getMyNfts()

    const items = await Promise.all(data.map(async i => {
      const tokenURI = await marketplaceContract.tokenURI(i.tokenId)
      const meta = await axios.get(tokenURI)
      let price = ethers.utils.formatUnits(i.price.toString(), 'ether')
      let item = {
        price,
        tokenId: i.tokenId.toNumber(),
        seller: i.seller,
        owner: i.owner,
        image: meta.data.image,
        name: meta.data.name,
        description: meta.data.description,
        tokenURI
      }
      return item
    }))
    setNfts(items)
    setLoadingState('loaded') 
  }
  function listNFT(nft) {
    router.push(`/sellBackNft?id=${nft.tokenId}&tokenURI=${nft.tokenURI}`)
  }
  if (loadingState === 'loaded' && !nfts.length) return (<h1 className="py-10 px-20 text-3xl">No NFTs owned</h1>)
  return (
    <div className="flex justify-center">
      <div className="p-4">
        <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-4 gap-4 pt-4">
          {
            nfts.map((nft, i) => (
              <div key={i} className="border overflow-hidden">
                <img src={nft.image} />
                <div className="p-4 ">
                  <p className="text-2xl font-bold ">{nft.name}</p>
                  <p className="text-2xl font-bold ">{nft.description}</p>
                  <p className="text-2xl font-bold ">Price - {nft.price} Eth</p>
                  {nft.owner === marketplaceAddress ? <p className="mt-4 w-full bg-gray-500 font-bold py-2 px-12 rounded">Listed on marketplace</p> : <button className="mt-4 w-full bg-gray-500 font-bold py-2 px-12 rounded" onClick={() => listNFT(nft)}>List on Marketplace</button>}
                </div>
              </div>
            ))
          }
        </div>
      </div>
    </div>
  )
}
import { ethers } from 'ethers'
import { useEffect, useState } from 'react'
import axios from 'axios'
import Web3Modal from 'web3modal'
import { marketplaceAddress } from '../config'
import nftMarket from '../build/contracts/nftMarket.json';



export default function Home() {

  const [nfts, setNfts] = useState([])
  const [loadingState, setLoadingState] = useState('not-loaded')
  useEffect(() => {
    loadNft()
  }, [])
  async function loadNft() {
    const provider = new ethers.providers.JsonRpcProvider()
    const contract = new ethers.Contract(marketplaceAddress, nftMarket.abi, provider)
    const data = await contract.showNFT()
    const nfts = await Promise.all(data.map(async i => {
      const tokenUri = await contract.tokenURI(i.tokenId)
      const meta = await axios.get(tokenUri)
      let price = ethers.utils.formatUnits(i.price.toString(), 'ether')
      let item = {
        price,
        tokenId: i.tokenId.toNumber(),
        seller: i.seller,
        owner: i.owner,
        image: meta.data.image,
        name: meta.data.name,
        description: meta.data.description,
      }
      return item
    }))
    setNfts(nfts)
    setLoadingState('loaded') 
  }
  async function buyNft(nft) {
    
    const web3Modal = new Web3Modal()
    const connection = await web3Modal.connect()
    const provider = new ethers.providers.Web3Provider(connection)
    const signer = provider.getSigner()
    const contract = new ethers.Contract(marketplaceAddress, nftMarket.abi, signer)
    const price = ethers.utils.parseUnits(nft.price.toString(), 'ether')   
    const transaction = await contract.sellNftOnMarket(nft.tokenId, {
      value: price
    })
    console.log({value: price})
    await transaction.wait()
    loadNft()
  }
  if (loadingState === 'loaded' && !nfts.length) 
  {
    return (<h1 className="px-20 py-10 text-3xl">No nft found</h1>)
  }
  return (
    <div className="flex justify-center container ">
    <div className="px-4" style={{ maxWidth: '1600px' }}>
      <div className="grid grid-cols-1  sm:grid-cols-2 lg:grid-cols-4 gap-4 pt-4">
        {
          nfts.map((nft, i) => (
            <div key={i} className="border shadow -xl overflow-hidden ">
              <img src={nft.image} />
              <div className="p-4 ">
                <p style={{ height: '20px' }} className="text-2xl text-red font-semibold">{nft.name}</p>
                <div style={{ height: '70px', overflow: 'hidden' }}><p className="text-black pt-5">{nft.description}</p></div>
              </div>
              <div className="p-4 ">
                <p className="text-2xl mb-4 font-bold ">{nft.price} ETH</p>
                <button className="w-full justify-center bg-gray-500 font-bold py-2 px-12 " onClick={() => buyNft(nft)}>Buy</button>
              </div>
            </div>
          ))
        }
      </div>
    </div>
  </div>
  )
}

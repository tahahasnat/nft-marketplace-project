// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/utils/Counters.sol";

contract nftMarket is ERC721URIStorage {
    using Counters for Counters.Counter;
    Counters.Counter private _nftId;
    Counters.Counter private _nftSold;

    uint256 nftListingFee = 0.021 ether;
    address payable owner;

    mapping(uint256 => NFT) public marketNftId;
    struct NFT {
      uint256 tokenId;
      address payable seller;
      address payable owner;
      uint256 price;
      bool isSold;
    }
    constructor() ERC721("Brand New NFT", "BNNFT") {
        owner = payable(msg.sender);
    }
    function getNftListingFee() public view returns (uint256) {
        return nftListingFee;
    }
   function mintNft(string memory tokenURI, uint256 price) public payable returns (uint) {
      _nftId.increment();

      uint256 newTokenId = _nftId.current();

      _mint(msg.sender, newTokenId);
      _setTokenURI(newTokenId, tokenURI);
      createNft(newTokenId, price);
      return newTokenId;
    }
    function createNft(uint256 tokenId,uint256 price) private {
      require(price > 0);
      require(msg.value == nftListingFee );

      marketNftId[tokenId] =  NFT(
        tokenId,
        payable(msg.sender),
        payable(address(this)),
        price,
        false
      );
      _transfer(msg.sender, address(this), tokenId);
    }
    function sellboughtNFT(uint256 nftId, uint256 nftPrice) public payable  {
        require(msg.value == nftListingFee);
        require(marketNftId[nftId].owner == msg.sender);
        marketNftId[nftId].isSold = false;
        marketNftId[nftId].price = nftPrice;
        marketNftId[nftId].seller = payable(msg.sender);
        marketNftId[nftId].owner = payable(address(this));
        _nftSold.decrement();
        _transfer(msg.sender, address(this), nftId);
    }
    function sellNftOnMarket(uint256 nftId) public payable {
        require(msg.sender !=  marketNftId[nftId].seller, "You can't Purchase your own NFT");
        uint price = marketNftId[nftId].price;
        require(msg.value == price);
        marketNftId[nftId].owner = payable(msg.sender);
        marketNftId[nftId].isSold = true;
        _nftSold.increment();
        
        _transfer(address(this), msg.sender, nftId);
        payable(owner).transfer(nftListingFee);
        payable(marketNftId[nftId].seller).transfer(msg.value);
        marketNftId[nftId].seller = payable(address(0));
    }
    function showNFT() public view returns(NFT[] memory) {
        uint nftCount = _nftId.current();
        uint unsoldNftCount = _nftId.current() - _nftSold.current();
        uint currentIndex = 0;

        NFT[] memory items = new NFT[](unsoldNftCount);
        for (uint i = 0; i < nftCount; i++) {
          if (marketNftId[i + 1].owner == address(this)) {
            uint currentId = i + 1;
            NFT storage currentItem = marketNftId[currentId];
            items[currentIndex] = currentItem;
            currentIndex += 1;
         }
        }
      return items;
    }
     function getMyNfts() public view returns (NFT[] memory) {
      uint totalItemCount = _nftId.current();
      uint itemCount = 0;
      uint currentIndex = 0;

      for (uint i = 0; i < totalItemCount; i++) {
        if (marketNftId[i + 1].owner == msg.sender || marketNftId[i + 1].seller == msg.sender) {
          itemCount += 1;
        }
      }

      NFT[] memory items = new NFT[](itemCount);
      for (uint i = 0; i < totalItemCount; i++) {
        if (marketNftId[i + 1].owner == msg.sender || marketNftId[i + 1].seller == msg.sender) {
          uint currentId = i + 1;
          NFT storage currentItem = marketNftId[currentId];
          items[currentIndex] = currentItem;
          currentIndex += 1;
        }
      }
      return items;
    }
}